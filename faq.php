<?php

 $cards = array(
	'card1' => array(
		'title' => "What is the Family Retreat like? We've never been.",
		'body' => "Family retreat is for any family size or make up.  It is one week - Monday night through Saturday morning.  We hit the ground running at family retreat and our main desire is for every family to have a time of fun, time together, alone time, and rest and relaxation.  We have intentionally designed our schedule to allow for all of those goals.  Monday Night is our kick off BBQ and we spend the evening in the main sanctuary worshiping the Lord, having fun, fellowship and getting to know one another. Through out the week there are times of bible study, fellowship, games (for families to participate together in) crafts and activities, down time, swimming pool, game room and snack bar time as well as evening study and gatherings.  We have an evening of bonfire and smores as well as movie nights and a ton more fun.  There are also many opportunities to just relax."
	),
	'card2' => array(
		'title' => "What meals are provided?",
		'body' => "Breakfast, Lunch and Dinner - all of the full days(Tuesday - Friday) Monday night dinner and Saturday morning breakfast.  There is also a snack bar that is open during our afternoon free time 1-5pm and also after the evening session which ends @ 9pm.  The snack bar remains open until 11pm.  There is also free coffee and muffins available early every morning from 6-8am for early risers."
 	
	),
    'card3' => array(
		'title' => 'What is the Conference Center like?',
		'body' => 'The Twin Peaks Conference center is nestled in the San Bernardino Mountains - right next door to Lake Arrowhead.  The elevation is approximately 4000 feet and the campus is tri-level. It is 100% wheel chair accessible.  There is a considerable amount of stairs and walking through out the week as the campus is close but multi-level - so you will get your workout for the week.  Wether young or old it is very doable and manageable.  The staff at Twin Peaks is very helpful if there are needs. This Conference Center is an 1960’s - remodeled retreat center and very nice.  For pictures visit - <a href=\"http://tpretreats.com/\">tpretreats.com</a> for more info.  All rooms are located next to the main center - which has the kitchen & sanctuary (Level 1 - bottom level - main level), children\'s ministry rooms - check in desk - fire places (Level 2), and snack bar and game room (Level 3)'
	),
    'card4' => array(
		'title' => 'I\'m gluten free, what can I eat?',
		'body' => 'We can make gluten free options - but we must know at the time of your registration and/or at least 1 month in advance.  The kitchen at the conference center can make accommodations for food sensitive people - as much as they are able.  The Conference Center will make their best effort to accommodate dietary restrictions as much as they are able to.'
	),
    'card5' => array(
		'title' => 'What are the rooms like?',
		'body' => 'The rooms accommodate 6 guests.  There are 2 bunk beds and one queen bed.  All bedding is provided as well as towels, soap and shampoo (hotel sample size :)  There are heaters and a/c units for each room, one full bath/shower and sink.  There are also decks for each room.  There are level 1 rooms (upstairs) and level 2 rooms (downstairs).  All rooms are located next to the main center - which has the kitchen & sanctuary (Level 1 - bottom level - main level), children\'s ministry rooms - check in desk - fire places (Level 2), and snack bar and game room (Level 3).'
	),
    'card6' => array(
		'title' => 'What if I have physical limitations?',
		'body' => 'We would be happy to accommodate you as best we are able. If you need assistance getting around the campus Please let us know what those limitations are so we can make the necessary arrangements prior to your arrival.'
	),
    'card7' => array(
		'title' => 'Do I have to attend all the sessions?',
		'body' => 'This is your retreat. We want you to be equipped, but also refreshed by your time away. So the choice is completely yours. We encourage you to be apart of as much as you are able but come and go with joy.'
	),
    'card8' => array(
		'title' => 'Can I bring my baby? What childcare is provided?',
		'body' => 'Yes - children of all ages are welcome.  Child care is provided for ages 0-6th grade - during the main sessions.  We also have a JH & HS fellowship that is available during our main sessions.'
	),
    'card9' => array(
		'title' => 'What is the schedule like?',
		'body' => 'Check out our schedule <a href="sched.html">here</a>.'
	),
	'card10' => array(
		'title' => 'Can we come for just some of the retreat if we can\'t stay all week or every day?',
		'body' => 'Yes you can - BUT you would have to pay for then entire week.  There isn’t a discount or arrangement to pay for individual days because our cost is set so low we don’t accommodate partial days.  We also would need to know what days you would be there so we can make sure your room is reserved and depending on when you come or go we can accommodate your access to your room.'
	),
	'card11' => array(
		'title' => 'Where and when do I check in?',
		'body' => 'Registration is on Monday at the main entrance / desk at 4pm-6pm - dinner starts at 6pm.  If you do arrive early (no earlier than 2pm - because retreat center staff is finishing preparations for retreat) you may be able to check in IF our staff is set up and ready to check in early - if not then 4pm.'
	),
);

?>

<!DOCTYPE html>
<html>
  <head>
    <!--<link rel="stylesheet" type="text/css" href="flexboxgrid/css/index.min.css">-->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,600" rel="stylesheet">
            <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"> 
    <link rel="stylesheet" type="text/css" href="main.css">
    <link rel="icon" type="image/png" href="images/fav.png"/>
    <link href="https://fonts.googleapis.com/css?family=Quattrocento" rel="stylesheet">
    <meta name="viewport" content="width=device-width">
     <title>Family Retreats</title>
  </head>
  <body>
 <div class="menu">
    <h1>CCCM Family Retreat</h1>
    <nav>
      <a href="index.html">Home</a>
      <a href="reg.html">Register</a>
      <a href="sched.html">Schedule</a>
      <a href="location.html">Location</a>
      <a href="speakers.html">Speakers</a>
      <a class="selected" href="faq.php">FAQ</a>
      <a href="photos.html">Photos</a>
      </nav>
 </div>
       <div class="web">
           <img src="images/family.jpg"/>
       </div>
        <div class="media">
           <img src="images/fam_phone.jpg"/>
       </div>
<div class="container">

          <h1>Contact Us</h1>
          <p>Please contact us with any questions or concerns you might have by filling out our contact form. We would love to hear from you!</p>

             <div class="contact">
                      <div id="mf_placeholder" 
                        data-formurl="//cts.cccm.com/machform/embed.php?id=163163" 
                        data-formheight="537"  
                        data-paddingbottom="10">
                     </div>
                      <script>
                        (function(f,o,r,m){
                          r=f.createElement('script');r.async=1;r.src=o+'js/mf.js';
                          m=f.getElementById('mf_placeholder'); m.parentNode.insertBefore(r, m);
                        })(document,'//cts.cccm.com/machform/');
                      </script>
             </div>

         <h1>Frequently Asked Questions</h1>

         <div id="accordion" role="tablist">

           <?php foreach($cards as $key=>$value): ?>
                  <div class="card">
                  <div class="card-header" role="tab" id="headingOne">
                          <h5 class="mb-0">
                          <a data-toggle="collapse" href="#<?php echo $key; ?>" role="button" aria-expanded="true" aria-controls="collapseOne">
                          <?php echo $cards[$key]['title']; ?>
                          </a>
                          </h5>
                      </div>
                  <div id="<?php echo $key; ?>" class="collapse <?php if($key == 'card1'): ?>show<?php endif; ?>" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                          <div class="card-body">
                          <?php echo $cards[$key]['body']; ?>        
                          </div>
                      </div>
                  </div>
           <?php endforeach; ?>

          </div>
    </div>


  <script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>

<div class="footer">
<footer>
  <p>&copy; Copyright 2017 <a href="http://cts.cccm.com/">CTS</a><a href="privacy.html"> Privacy Policy</a></p>
</footer>
</div>
</body>
</html>
